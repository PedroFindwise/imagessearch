import { Injectable } from '@angular/core';
import { Client } from 'elasticsearch-browser';
import { environment } from '../environments/environment';
import * as elasticsearch from 'elasticsearch-browser';

@Injectable({
  providedIn: 'root'
})
export class ElasticsearchService {

  private client: Client;
  host: string = environment.settings.host;
  auth: string = environment.settings.auth;
  port: number = environment.settings.port;

  private queryalldocs = {
    "size": 2000,
    'query': {
      'match_all': {}
    }
  };

  constructor() {
    if (!this.client) {
      this._connect();
    }
  }

  private _connect() {
    this.client = new elasticsearch.Client({
      host: [
    {
      host: this.host,
      auth: this.auth,
      protocol: 'https',
      port: this.port
    }
  ]
    });
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
      body: 'hello grokonez!'
    });
  }

  getAllDocuments(_index, _type): any {
    return this.client.search({
      index: _index,
      type: _type,
      body: this.queryalldocs
    });
  }
  getPageDocuments(_index, _type, _start): any {
    return this.client.search({
      index: _index,
      type: _type,
      body: {'from' : _start, 'size' : 20,
            'query': {
              'match_all': {}
              }
            },
    });
  }
  getFilterDocuments(_index, _type, _value, _start): any {
    return this.client.search({
      index: _index,
      type: _type,
      body: {'from' : _start, 'size' : 20,
            'query': {
              "constant_score" : {
                 "filter" : {
                    "bool" : {
                      "should" : [
                        { 'match': {'LinkedInSkill': _value} },
                        { 'term': {'Type': _value} },
                        { 'term': {'Id': _value} },
                        { 'match': {'Name': _value} },
                        { 'term': {'Status': _value} }
                      ]
                    }
                  }
              }
            }
      },
    });
  }
  updateDocument(_index, _type, _id, _value): any {
    return this.client.update({
      index: _index,
      type: _type,
      id: _id,
      body: {
        doc: {
          'LinkedInSkill': _value[0],
          'Type': _value[1],
          'Id': _value[2],
          'Name': _value[3],
          'Status': _value[4]
        }
      },
    });
  }
  getProduct(_index, _type, _value, _size): any {
    return this.client.search({
      index: _index,
      type: _type,
      body: {'size' : _size,
            'query': {
              "match": { "search_terms" : _value }
            }
      },
    });
  }
}
