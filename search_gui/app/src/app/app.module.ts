import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule, MatInputModule, MatAutocompleteModule, MatTableModule, MatFormFieldModule, MatDialogModule, MatOptionModule, MatSelectModule, MatButtonModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ImagesListComponent } from './images-list/images-list.component';
import { ElasticsearchService } from './elasticsearch.service';
import { EditorButtonComponent } from './editor-button/editor-button.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesListComponent,
    EditorButtonComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonModule,
    MatAutocompleteModule
  ],
  providers: [
    ElasticsearchService
  ],
  entryComponents: [
    EditorButtonComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
