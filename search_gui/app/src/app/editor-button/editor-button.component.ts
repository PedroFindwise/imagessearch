import { Component, OnInit, Inject } from '@angular/core';
import { ElasticsearchService } from '../elasticsearch.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {environment} from "../../environments/environment";

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'editor',
  templateUrl: './editor-button.component.html',
  styleUrls: ['./editor-button.component.css']
})
export class EditorButtonComponent implements OnInit {
  options: FormGroup;
  id: string;
  result: string;
  newData: any[];
  filteredNames: any;

  constructor(private es: ElasticsearchService,
              public thisDialogRef: MatDialogRef<EditorButtonComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              fb: FormBuilder)
              {
              this.options = fb.group({
                type: data._source.Type,
                status: data._source.Status,
                linkedinskill: data._source.LinkedInSkill,
                id: [{value: data._source.Id, disabled: true}],
                name: data._source.Name,
              });
              }

  ngOnInit() {
  }

  updateProductName(input: string) {
    this.es.getProduct(environment.settings.index.products_search, 'productgraphsearchmodel', input, 10)
      .then(response => {
        this.filteredNames = response.hits.hits
      });
  }
  getDocument(name) {
    this.es.getProduct(environment.settings.index.products_search, 'productgraphsearchmodel', name.option.value, 1)
      .then(response => {
        this.options.controls["id"].setValue(response.hits.hits[0]._source.id);
        this.options.value.name = response.hits.hits[0]._source.term;
      });
  }

  onCloseConfirm() {
    this.thisDialogRef.close('Cancelled');
  }



  onSubmitConfirm() {
    this.id = this.data._id;
    this.newData = [this.options.value.linkedinskill,
                    this.options.value.type,
                    this.options.controls["id"].value,
                    this.options.value.name,
                    this.options.value.status];
    this.es.updateDocument(environment.settings.index.product_match, 'linkedin_productmatch', this.id, this.newData)
        .then(response => {
          this.result = response;
          console.log(response);
        }, error => {
          console.error(error);
        });
    this.thisDialogRef.close(this.newData);
  }

}
