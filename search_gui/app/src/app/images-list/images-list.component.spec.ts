import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkedinEditorComponent } from './linkedin-editor.component';

describe('LinkedinEditorComponent', () => {
  let component: LinkedinEditorComponent;
  let fixture: ComponentFixture<LinkedinEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LinkedinEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkedinEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
