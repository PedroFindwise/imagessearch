import { Component, OnInit, ChangeDetectorRef, NgModule } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ElasticsearchService } from '../elasticsearch.service';
import { EditorButtonComponent } from '../editor-button/editor-button.component';
import { MatDialog } from '@angular/material';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.css']
})
export class ImagesListComponent implements OnInit {
  start: number = 0;
  pages : number = 4;
  pageSize : number = 20;
  pageNumber : number = 0;
  currentIndex : number = 1;
  pageStart : number = 1;
  pagesIndex : Array<number>;
  isConnected = false;
  isFilterOn = false;
  status: string;
  items: any;
  inputName : string = "";
  max_hits: number;
  dialogResult = "";
  newData: any[];

  constructor(private es: ElasticsearchService, private cd: ChangeDetectorRef, public dialog: MatDialog) {
    this.isConnected = false;
  }
  init(){
      this.es.getPageDocuments(environment.settings.index.product_match, 'linkedin_productmatch', 0)
        .then(response => {
          this.items = response.hits.hits;
          this.max_hits = response.hits.total;
          this.pageNumber = parseInt(""+ (this.max_hits / this.pageSize));
            if(this.max_hits % this.pageSize != 0){
              this.pageNumber ++;
            }
            if(this.pageNumber  < this.pages){
              this.pages =  this.pageNumber;
            }
          this.pagesIndex =  this.fillArray();
        }, error => {
          console.error(error);
        }).then(() => {
          console.log('Show Linkedin Skills Completed!');
        });
   }

   fillArray(): any{
      var obj = new Array();
      for(var index = this.pageStart; index< this.pageStart + this.pages; index ++) {
                  obj.push(index);
      }
      return obj;
   }

   refreshItems(){
    if (this.isFilterOn == false) {
      this.es.getPageDocuments(environment.settings.index.product_match, 'linkedin_productmatch', this.start)
        .then(response => {
          this.items = response.hits.hits;
          this.pagesIndex =  this.fillArray();
        });
    } else {
        this.es.getFilterDocuments(environment.settings.index.product_match, 'linkedin_productmatch', this.inputName, this.start)
          .then(response => {
            this.items = response.hits.hits;
            this.pagesIndex =  this.fillArray();
          });

      }
   }

   FilterByName(input){
      this.inputName=input;
      if(this.inputName != "") {
        this.isFilterOn = true;
        this.es.getFilterDocuments(environment.settings.index.product_match, 'linkedin_productmatch', this.inputName, 0)
        .then(response => {
          this.items = response.hits.hits;
          this.max_hits = response.hits.total;
          this.pageStart = 1;
          this.currentIndex = 1;
          this.pageNumber = parseInt(""+ (this.max_hits / this.pageSize));
            if(this.max_hits % this.pageSize != 0){
              this.pageNumber ++;
            }
            if(this.pageNumber  < this.pages){
              this.pages =  this.pageNumber;
            }
          this.pagesIndex =  this.fillArray();
        }, error => {
          console.error(error);
      });
      } else {
          this.start = 0;
          this.pages = 4;
          this.pageStart = 1;
          this.currentIndex = 1;
          this.init();
        }

   }

  ngOnInit() {
    this.es.isAvailable().then(() => {
      this.status = 'OK';
      this.isConnected = true;
    }, error => {
      this.status = 'ERROR';
      this.isConnected = false;
      console.error('Server is down', error);
    }).then(() => {
      this.cd.detectChanges();
    });

    this.init();

  }

  prevPage(){
      if(this.currentIndex>1){
         this.currentIndex --;
         this.start = this.start - 20;
      }
      if(this.currentIndex < this.pageStart){
         this.pageStart = this.currentIndex;
         this.start = this.start - 20;
      }
      this.refreshItems();
   }
   nextPage(){
      if(this.currentIndex < this.pageNumber){
            this.currentIndex ++;
            this.start = this.start + 20;
      }
      if(this.currentIndex >= (this.pageStart + this.pages)){
         this.pageStart = this.currentIndex - this.pages + 1;
         this.start = this.start + 20;
      }

      this.refreshItems();
   }
    setPage(index : number){
         this.currentIndex = index;
         this.start = 20*(index-1);
         this.refreshItems();
    }

    openEditor(tosend) {
      console.log(tosend);
      let dialogRef = this.dialog.open(EditorButtonComponent, {
        width: '800px',
        data: tosend
      });
      dialogRef.afterClosed().subscribe(result => {
        if ((typeof result !== 'undefined') && (result != 'Cancelled') ){
          tosend._source.LinkedInSkill = result[0];
          tosend._source.Type = result[1];
          tosend._source.Id = result[2];
          tosend._source.Name = result[3];
          tosend._source.Status = result[4];
        };
      });
    }
    blackListSubmit(datatoapprove) {
    datatoapprove._source.Status = 'accepted';
    datatoapprove._source.Type = 'BlacklistItem';
    datatoapprove._source.Id = null;
    datatoapprove._source.Name = null;

    this.newData = [datatoapprove._source.LinkedInSkill,
                    datatoapprove._source.Type,
                    datatoapprove._source.Id,
                    datatoapprove._source.Name,
                    datatoapprove._source.Status];
    this.es.updateDocument(environment.settings.index.product_match, 'linkedin_productmatch', datatoapprove._id, this.newData)
      .then(response => {
        console.log(response);
      }, error => {
        console.error(error);
      });
  }
  approveSubmit(datatoapprove) {
    datatoapprove._source.Status = 'accepted';
    this.newData = [datatoapprove._source.LinkedInSkill,
                    datatoapprove.Type,
                    datatoapprove.Id,
                    datatoapprove.Name,
                    'accepted'];
    this.es.updateDocument(environment.settings.index.product_match, 'linkedin_productmatch', datatoapprove._id, this.newData)
      .then(response => {
        console.log(response);
      }, error => {
        console.error(error);
      });
  }
}


