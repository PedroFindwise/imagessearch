export const environment = {
  name: 'production',
  production: true,
  settings: {
    host: "",
    auth: "",
    port: 0,
    index: {
      product_match: "",
      products_search: ""
    }
  }
};
